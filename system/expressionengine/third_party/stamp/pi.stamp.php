<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include( PATH_THIRD.'stamp/ee_fallback.php' );

$plugin_info = array(
	'pi_name' => 'Stamp',
	'pi_version' => '1.1',
	'pi_author' => 'Aaron Waldon',
	'pi_author_url' => 'https://www.causingeffect.com',
	'pi_description' => 'A tiny plugin for adding timestamps to filenames.',
	'pi_usage' => Stamp::usage()
);

class Stamp
{
	/**
	 * Adds a timestamp to a filename.
	 *
	 * @return string
	 */
	public function er()
	{
		//get the mode, defaults to file
		$mode = ee()->TMPL->fetch_param('mode','file');

		//get the relative file path
		$fileName = ee()->TMPL->tagdata;
		if ( empty($fileName) )
		{
			return ee()->TMPL->no_results();
		}

		//determine the document root
		$documentRoot = isset( $_SERVER['DOCUMENT_ROOT'] ) ? $_SERVER['DOCUMENT_ROOT'] : FCPATH;
		$documentRoot = $this->determine_setting( 'public_root', $documentRoot, true, true, false );

		//determine the full path
		$filePath = $this->remove_duplicate_slashes($documentRoot.'/'.$fileName);

		//make sure the full path exists
		if (! file_exists($filePath))
		{
			return ee()->TMPL->no_results();
		}

		//get the path info
		$path_parts = pathinfo($this->remove_duplicate_slashes('/'.$fileName));

		if ($mode=='file')
		{
			return $path_parts['dirname'] . '/' . $path_parts['filename'] . '.' . filemtime($filePath) . '.' . $path_parts['extension'];
		}
		else if ($mode=='folder')
		{
			return $path_parts['dirname'] . '/' . filemtime($filePath) . '/' . $path_parts['filename'] . '.' . $path_parts['extension'];
		}
		else if ($mode=='query')
		{
			return $path_parts['dirname'] . '/' . $path_parts['filename'] . '.' . $path_parts['extension'] . '?ts=' . filemtime($filePath);
		}
		else if ($mode=='tsonly')
		{
			return filemtime($filePath);
		}

		//if we made it this far, then let's return the no_results content
		return ee()->TMPL->no_results();
	}


	/**
	 * Determines the given setting by checking for the param, and then for the global var, and then for the config item.
	 * @param string $name The name of the parameter. The string 'ce_image_' will automatically be prepended for the global and config setting checks.
	 * @param string $default The default setting value
	 * @param bool $check_global_vars Whether or not to check the global variables array for the setting.
	 * @param bool $check_config Whether or not to check the config for the setting.
	 * @param bool $check_param Whether or not to check the template parameter.
	 * @return string The setting value if found, or the default setting if not found.
	 */
	protected function determine_setting( $name, $default = '', $check_global_vars = true, $check_config = true, $check_param = true )
	{
		$long_name = 'stamp_' . $name;
		if ( $check_param && ee()->TMPL->fetch_param( $name ) !== FALSE ) //param
		{
			$default = ee()->TMPL->fetch_param( $name );
		}
		else if ( $check_global_vars && isset( ee()->config->_global_vars[ $long_name ] ) && ee()->config->_global_vars[ $long_name ] !== FALSE ) //first check global array
		{
			$default = ee()->config->_global_vars[ $long_name ];
		}
		else if ( $check_config && ee()->config->item( $long_name ) !== FALSE ) //then check config
		{
			$default = ee()->config->item( $long_name );
		}

		return $default;
	}

	/**
	 * Removes double slashes, except when they are preceded by ':', so that 'http://', etc are preserved.
	 *
	 * @param string $str The string from which to remove the double slashes.
	 * @return string The string with double slashes removed.
	 */
	private function remove_duplicate_slashes( $str )
	{
		return preg_replace( '#(?<!:)//+#', '/', $str );
	}


	/**
	 * Returns usage instructions for the plugin to the EE control panel.
	 *
	 * @return string
	 */
	public static function usage()
	{
		ob_start();
		?>
Use it like this:

<script src="{exp:stamp:er}/assets/build/js/scripts.js{/exp:stamp:er}"></script>

Which results in:

<script src="/assets/build/js/scripts.1399647655.js"></script>

Then set up URL rewriting:

Apache:

	# Rewrites asset versioning, ie styles.1399647655.css to styles.css.
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule ^(.+)\.(\d{10})\.(js|css)$ $1.$3 [L]  # /assets/build/js/scripts.1399647655.js
	# RewriteRule ^(.+)/(\d{10})/(.+)\.(js|css)$ $1/$3.$4 [L]  # /assets/build/js/1399647655/scripts.js

nginx:

	location @assetversioning {
		rewrite ^(.+)\.[0-9]+\.(css|js)$ $1.$2 last;  # /assets/build/js/scripts.1399647655.js
		# rewrite ^(.+)/([0-9]+)/(.+)\.(js|css)$ $1/$3.$4 last;  # /assets/build/js/1399647655/scripts.js
	}

	location ~* ^/assets/.*\.(?:css|js)$ {
		try_files $uri @assetversioning;
		expires max;
		add_header Pragma public;
		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
	}

		<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}
}
/* End of file pi.stamp.php */
/* Location: ./system/expressionengine/third_party/stamp/pi.stamp.php */